import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('grupos-adm-page', 'Integration | Component | grupos adm page', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{grupos-adm-page}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#grupos-adm-page}}
      template block text
    {{/grupos-adm-page}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
