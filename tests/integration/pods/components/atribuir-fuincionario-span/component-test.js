import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('atribuir-funcionario-span', 'Integration | Component | atribuir funcionario span', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{atribuir-funcionario-span}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#atribuir-funcionario-span}}
      template block text
    {{/atribuir-funcionario-span}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
