import Mirage from 'ember-cli-mirage';

function limparToken(Authorization){
  const token = Authorization.split(' ');
  if(token[1]){
    return token[1];
  }else{
    return null;
  }
}
export default function() {

  this.get("grupos");
  this.get("produtos");

  //heee tem include do user com grupo
  this.get("users/:id");
  this.post("tickets");
  this.get("tickets",function({tickets},request){
    if(request.queryParams && request.queryParams.include == undefined){
      return tickets.where({grupoId:request.queryParams.grupo});
    }else{
      return tickets.all();
    }
  });
  this.patch("tickets/:id");
  this.get("tickets/:id");

  this.get("/users",function({users},request){
    if(request.queryParams){
      return users.where(request.queryParams)
    }else{
      return users.all();
    }
  });
  this.del('/intgrup',function({users,grupos},request){
    const JSONBody = JSON.parse(request.requestBody);
    //busca o usuario que deve ser remolvido
    const iduser = JSONBody.user.iduser;
    const user = users.find(iduser);
    //busca o grupo que o usuario deve ser remolvido
    const idgrupo =  JSONBody.grupo.idgrupo;
    const grupo = grupos.find(idgrupo);
    const preremove = grupo.userIds.indexOf(iduser)
    if(preremove >= 0 ){
      grupo.userIds.splice(grupo.userIds.indexOf(iduser));
      return grupo;
    }else{
       return new Mirage.Response(422,{});
    }
    //grupo.userIds.removeObject(user);
    //grupo.pushObject(iduser);


    //let user = users.find();
  });
  this.post("intgrup",function({users,grupos},request){
    const JSONBody = JSON.parse(request.requestBody);
    //busca o usuario que deve ser adicionado
    const iduser = JSONBody.user.iduser;
    const user = users.find(iduser);
    //busca o grupo que o usuario deve ser adicionado
    const idgrupo =  JSONBody.grupo.idgrupo;
    const grupo = grupos.find(idgrupo);
    grupo.userIds.push(iduser);
    grupo.update(grupo.attr);
    // retornar os dados do grupo
    return grupo;
  });
  this.get('/me',function({users},request){
    let token = limparToken(request.requestHeaders.Authorization);
    return users.find(token);
  });

  this.post('/api/token-auth',function({users},request){
    var params = JSON.parse(request.requestBody);
    let userdata = null;
    let valid = false;
    let reson = '';
    // verifico se os dados chegaram
    if( params.email && params.password){
      userdata = users.findBy({email: params.email});
      // verifico se existe o usuario no banco
      if(userdata){
      // comparo as senhas
        if(userdata.password == params.password){
          valid = true;
        }else{
          reson = 'Invalid Password';
        }
      }else{
        reson = 'invalid email';
      }
    }else{
      reson = 'Empty';
    }
    if (valid) {
    //caso verdade ele retorna o jwt correspondente
      return {
        token : userdata.id,
        // user: user
      };
    }else{
      return new Mirage.Response(422,{},reson);
    }
  });
}
