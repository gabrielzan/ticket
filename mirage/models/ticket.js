import { Model,belongsTo,hasMany } from 'ember-cli-mirage';

export default Model.extend({
  cliente: belongsTo('user'),
  produto: belongsTo('produto'),
  atribuido: belongsTo('user'),
  grupo: belongsTo('grupo'),
  historico: hasMany('historico')
});
