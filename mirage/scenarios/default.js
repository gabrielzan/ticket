export default function( server ) {

  let folha = server.create('grupo',{
    nome:"suporte folha"
  });
  let financeiro = server.create('grupo',{
    nome:"suporte financeiro"
  });
  let adm = server.create('grupo',{
    nome:"administradores"
  });
  let central = server.create('produto',{
    nome:"Sistema Central"
  });
  let condominio = server.create('produto',{
    nome:"Condomínio Auto-Gestão"
  });
  let fiscal = server.create('produto',{
    nome:"Fiscal e Contábil"
  });
  let pagamento = server.create('produto',{
    nome:"Folha de Pagamento"
  });
  let Uadm = server.create('user',{
    nome:"admin",
    id:1,
    email:"admin",
    adm:true,
    grupos:[adm,folha],
    cliente:false,
    funcionario:true,
    password:"admin"
  });
  let Ufuncionario = server.create('user',{
    id:2,
    nome:"funcionario",
    email:"funcionario",
    adm:false,
    grupos:[financeiro],
    cliente:false,
    funcionario:true,
    password:"funcionario"
  });
  server.create('user',{
    id:9,
    nome:"joriscleiton",
    email:"joriscleiton@yahool.com",
    adm:false,
    grupos:[financeiro,folha],
    cliente:false,
    funcionario:true,
    password:"joriscleiton"
  });
  server.create("user",{
    email: "preto",
    nome:'preto',
    password: "123456",
    funcionario:true,
    cliente:false,
    adm:false,
    avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/chrisstumph/128.jpg"
  });
  let Ucliente = server.create('user',{
    id:3,
    nome:"cliente",
    email:"cliente",
    avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/wikiziner/128.jpg",
    adm:false,
    cliente:true,
    funcionario:false,
    password:"cliente"
  });
  server.create('ticket',{
    cliente: Ucliente,
    produto: condominio,
    atribuido: Uadm,
    grupo: folha,
    assunto: "roubei um avião mais nao sei como dirigir",
    descricao: "estou saindo aq do afeganistão com um avião que eu comprei mais nao sei como pousar e nem parar o avião e nem a altitude ja estou chegando proximo de 2 torres e nao sei como parar, chegaram uns malucos aqui batendo na porta, gritando balacuba e tals, dai eu peguei e chamei minha secretaria, dona criselda, um amor de pessoa, tem dois filhos, dona criselda comprou um celular recentemente agora fica vendo as noticias o tempo todo, mas dai quando a galera comecou a bater ela veio e dai eu olhei no gps dela pra saber onde agente tava, tava longe pra burro e ela ainda tava quase sem bateria, dai eu lembrei que tava num avião e devia ter um (GPS?) carregador ali, dai eu fui la e peguei o carregador pra ela e tals ...",
    anexos: '',
    comentarios: '',
    historico:[
      server.create("historico",{
        tipo:"cadastro",
        titulo:"Ticket foi Cadastrado",
        descricao:"o Ticket Foi Cadastrado por Gabriel",
        horario: new Date(),
        data: {},
        referencias: [{tipo:"usuario",data:{nome:"Gabriel",id:"789"}}]
      })
    ]
  });
  server.create('ticket',{
    cliente: Ucliente,
    produto: fiscal,
    atribuido: Ufuncionario,
    grupo: financeiro,
    assunto: "estamos perdidos no meio de jerusalem",
    descricao: "tamo levando uns incenso ouro e pano para um bebe pq faz mt sentido mais o gps parou de funcionar e agora estamos seguindo uma estrela ajuda qui po",
    anexos: '',
    comentarios: ''
  });
   server.create('ticket',{
    cliente: Ucliente,
    produto: pagamento,
    atribuido: Ufuncionario,
    grupo: financeiro,
    assunto: "meu deus do ceu",
    descricao: "estava gravando uma reportagem ai começou um tiroteiro sinistro e acabei esquecendo minha senha de login como faso para recuperar ela ?",
    anexos: '',
    comentarios: ''
  });
  server.create('ticket',{
    cliente: Ucliente,
    produto: central,
    atribuido: Ufuncionario,
    grupo: financeiro,
    assunto: "tava fazendo umas parada e bugou",
    descricao: "gosto de maçã verde descascada com suco de uva verde pq a roxa me faz vomitar",
    anexos: '',
    comentarios: ''
  });
  // server.createList('ticket',5);
  server.create("user",{
    email: "preto",
    password: "123456",
    funcionario:true
  });
  server.create('grupo',{
    nome:"suporte"
  });
}
