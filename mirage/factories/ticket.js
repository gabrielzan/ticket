import { Factory,faker } from 'ember-cli-mirage';

export default Factory.extend({
  dataInclusao: () => {return faker.date.recent()},
  status: () => {
    let random = faker.random.number({min:0,max:2});
    let retorno = '';
    switch(random){
      case 0:
      retorno = 'Fechado';
      break;
      case 1:
      retorno = 'Em andamento';
      break;
      case 2:
      retorno = 'Aberto';
      break;
    }
    return retorno;
  },
  prioridade: () => {
    let random = faker.random.number({min:0,max:2});
    let retorno = '';
    switch(random){
      case 0:
      retorno = 'Media';
      break;
      case 1:
      retorno = 'Baixa';
      break;
      case 2:
      retorno = 'Alta';
      break;
    }
    return retorno;
  },
  assunto: () => faker.lorem.sentence(),
  descricao: () => {return "1"},
});
