import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin,{
  session: service(),
  authenticationRoute:"login",
  beforeModel(){
    let loadUser = this.get('session.loadUser');
    return loadUser.perform();
  },
  model(){
    let userId = this.get('session.user.id');
    return this.store.findRecord('user',userId,{include:'grupos'});
  }
});
