import Route from '@ember/routing/route';
import {hash} from 'rsvp';

export default Route.extend({
  model() {
    let funcionarios = this.store.query('user', {funcionario: true});
    let grupos = this.store.findAll('grupo', {include: "users"});
    return hash({funcionarios,grupos});
  }
});
