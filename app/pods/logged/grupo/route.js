import Route from '@ember/routing/route';
import {hash} from 'rsvp';

export default Route.extend({
  model(parms) {
    let tickets = this.store.query('ticket', {grupo: parms.id});
    let grupos = this.store.findAll('grupo');
    let clientes = this.store.query('user', {cliente: true});
    return hash({tickets,grupos,clientes});
  }
});
