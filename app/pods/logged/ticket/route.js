import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model(parms) {
    const usuarios = this.store.findAll('user');
    const ticket = this.store.findRecord('ticket', parms.id,{include:'cliente,produto,atribuido,grupo'});
    const grupo = this.store.findAll('grupo');
    return RSVP.hash({usuarios,ticket,grupo});
  }
});
