import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model(){
// atribuido = usuario logado
    const cliente = this.store.query('user',{cliente: true});
    const funcionarios = this.store.query('user',{funcionario: true});
    const grupo = this.store.findAll('grupo');
    const produto = this.store.findAll('produto');
    return RSVP.hash({cliente,grupo,produto,funcionarios});
  }
});
