import Route from '@ember/routing/route';
import {hash} from 'rsvp';

export default Route.extend({
  model() {
    let tickets = this.store.findAll('ticket',{include:'cliente,produto,atribuido,grupo,historico'});
    let grupos = this.store.findAll('grupo');
    let clientes = this.store.query('user', {cliente: true});
    return hash({clientes,tickets,grupos});
  }
});
