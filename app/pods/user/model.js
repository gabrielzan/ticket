import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany  } from 'ember-data/relationships';

export default Model.extend({
  email: attr('string'),
  nome: attr('string'),
  grupos: hasMany('grupo', {inverse: 'users'}),
  adm: attr('boolean'),
  cliente: attr('boolean'),
  funcionario: attr('boolean'),
  password: attr('string'),
  avatar: attr('string')
});
