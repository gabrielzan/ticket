import Component from '@ember/component';
import { inject } from '@ember/service';

export default Component.extend({
    session: inject(),
    store: inject(),
    grupoSelec:'',
    actions:{
      removerUserGrupo(user,idgrupo){
        let store = this.get('store');
        let adapterUser = store.adapterFor("user");
        let resposta = adapterUser.removeDoGrupo(idgrupo.id,user.id);
         resposta.then( (grupo) => {
          grupo.data.type = 'grupo'
          store.push(grupo);
          let selfUser = this.get('store').peekRecord('user',user.id);
          idgrupo.get('users').removeObject(selfUser);
         }).catch(()=>{
          location.reload();
         });
      },
      abreSpan(grupo){
        this.set("grupoSelec",grupo)
      }
    }
});
