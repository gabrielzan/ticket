import Component from '@ember/component';

export default Component.extend({
  actions: {
    handleChange(e) {
      let dropdown = e.target;
      let val = dropdown.options[dropdown.selectedIndex].value;
      let onchange = this.get('changeAction');

      if (onchange) {
        onchange(val, e, this);
      } else {
        return [val, e, this];
      }
    }
  }
});
