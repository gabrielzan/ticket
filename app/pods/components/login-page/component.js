import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  session: service(),
  email: "",
  password: "",
  actions: {
    singin(){
      this.get('session').authenticate('authenticator:token',{email:this.get("email"),password:this.get("password")}).catch((reason) => {
        this.set('errorMessage', reason.text || reason);
      });
    }
  }
});
