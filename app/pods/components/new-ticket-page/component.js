import Component from '@ember/component';
import { A } from '@ember/array';
import { inject } from '@ember/service';

export default Component.extend({
  store: inject(),
  session: inject(),
  router: inject(),
  clienteAss:'',
  produtoAss:'',
  prioridades:A(["Media", "Baixa", "Alta"]),
  prioridade: '',
  Error:'',
  grupoAtt:'',
  assunto:'',
  descricao:'',
  actions: {
    saveTicket() {
      let clienteModel = this.get('cliente');
      let cliente = this.get('clienteAss');
      let clienteAss = clienteModel.findBy('id',cliente);
      let grupos = this.get('grupo');
      let grupo = this.get('grupoAtt');
      let grupoAss = grupos.findBy('id',grupo);
      let produtoModel = this.get('produto');
      let produto = this.get('produtoAss');
      let produtoAss = produtoModel.findBy('id',produto);
      let funcionarios = this.get('funcionarios');
      let meuId = this.get('session.user.id');
      let eu = funcionarios.findBy('id',meuId);

      let newModel = this.get('store').createRecord('ticket',{
          dataInclusao: Date.now(),
          cliente: clienteAss,
          produto: produtoAss,
          status: 'Aberto',
          prioridade: this.get('prioridade'),
          atribuido: eu,
          grupo: grupoAss,
          assunto: this.get('assunto.target.value'),
          descricao: this.get('descricao.target.value'),
      });
      if(clienteAss && produtoAss && this.get('prioridade') && grupoAss && this.get('assunto') && this.get('descricao')){
      newModel.save().then( () => {
        this.get('router').transitionTo('logged.tickets');
      });

      }else{
        this.set('Error',"preencha todos os campos")
      }

    },
  }
});
