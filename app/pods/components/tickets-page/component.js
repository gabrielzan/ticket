import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { A } from '@ember/array';
import { computed } from '@ember/object';

export default Component.extend({
  session: service(),

  filtro_status: A(["Todos","Fechado", "Em andamento", "Aberto"]),
  filtro_data: A(["Sempre",'hoje',"Esta semana"]),
  filtro_prioridade:A(['Todas',"Media", "Baixa", "Alta"]),
  recente: true,
  filtro_cliente: computed('clientes',function(){
    let nomes = [];
    nomes.push("Todos");
    this.get("clientes").forEach((usuario)=>{
      nomes.push(usuario.get("nome"));
    })
    return nomes;
  }),
  filtro_grupo: computed('grupos',function(){
    let nomes = [];
    nomes.push("Todos");
    this.get("grupos").forEach((usuario)=>{
      nomes.push(usuario.get("nome"));
    })
    return nomes;
  }),
  filtro_status_atual:"Todos",
  filtro_data_atual:"Sempre",
  filtro_cliente_atual:"Todos",
  filtro_grupos_atual:"Todos",
  filtro_prioridade_atual:"Todas",
  lista_ordenada_data: computed("tickets","recente",function(){
    let result = this.get('tickets').sortBy("dataInclusao");
    if (this.get('recente')) {
      return result.reverse()
    }else{
      return result;
    }
  }),
  lista: computed(
    'filtro_status_atual',
    'filtro_data_atual',
    'filtro_cliente_atual',
    'filtro_grupos_atual',
    'filtro_prioridade_atual',
    'lista_ordenada_data',
    'tickets.[]',
    function(){
      let lista = this.get('lista_ordenada_data');
      if(this.get("filtro_cliente_atual") !== "Todos"){
        lista = lista.filterBy("cliente.nome",this.get("filtro_cliente_atual"))
      }
      if(this.get("filtro_data_atual") !== "Sempre"){
        // lista = lista.filterBy("",this.get("filtro_data_atual"))
      }
      if(this.get("filtro_prioridade_atual") !== "Todas"){
        lista = lista.filterBy("prioridade",this.get("filtro_prioridade_atual"))
      }
      if(this.get("filtro_status_atual") !== "Todos"){
        lista = lista.filterBy("status",this.get("filtro_status_atual"))
      }
      return lista;
  }),
  actions:{
    changeStatus(newStatus){
      this.set('filtro_status_atual',newStatus);
    },
    changeData(newData){
      this.set('filtro_data_atual',newData);
    },
    changeCliente(newCliente){
      this.set('filtro_cliente_atual',newCliente);
    },
    changeGrupo(newGrupo){
      this.set('filtro_grupos_atual',newGrupo);
    },
    changePrioridade(newPrioridade){
      this.set('filtro_prioridade_atual',newPrioridade);
    },
    ChangeDataOrdenacaoAtual(ord_data){
      this.set('ordenacao_data_atual',ord_data);
    },
  }
});
