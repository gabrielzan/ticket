import Component from '@ember/component';
import { A } from '@ember/array';
import { inject as service } from '@ember/service';


export default Component.extend({
  session: service(),
  status: A(["Fechado", "Em andamento", "Aberto"]),
  prioridade:A(["Media", "Baixa", "Alta"]),
  actions:{
   editGrupo(id){
    let modelTicket = this.get("ticket");
    let modelGrupo = this.get("grupos");
    let procura = modelGrupo.findBy('id',id);
    this.set("ticket.grupo",procura)
    modelTicket.save();
   },
   mudaPrioriedade(newPrioridade){
    let modelTicket = this.get('ticket');
    this.set("ticket.prioridade",newPrioridade);
    modelTicket.save();
   },
   mudaStatus(newStatus){
    let modelTicket = this.get('ticket');
    this.set("ticket.status",newStatus);
    modelTicket.save();
   },
   editFuncionario(id){
    let modelUser = this.get("usuarios");
    let modelTicket = this.get("ticket");
    let procura = modelUser.findBy('id',id);
    this.set('ticket.atribuido',procura);
    modelTicket.save();
   }
  }
});
