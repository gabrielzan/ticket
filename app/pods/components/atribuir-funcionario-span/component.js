import Component from '@ember/component';
import  { computed } from '@ember/object';
import { inject } from '@ember/service';
import { alias } from '@ember/object/computed';
export default Component.extend({
  store: inject(),
  users:computed('grupo', function() {
    let funcionarios = this.get('funcionarios');
    let grupo = this.get("grupo.users");
    let lista = [];
    funcionarios.forEach((usuario) =>{
      var have = false;
        grupo.forEach((usergrupo) =>{
          if (usergrupo.id == usuario.id){
            have = true;
          }
      });
        if(!have){
          lista.push(usuario);
        }
    });
    return lista;
  }),
  actions:{
    addnogrupo(user,grupo){
        let store = this.get('store');
        let adapterUser = store.adapterFor("user");
        let resposta = adapterUser.adicionaNoGrupo(grupo.id,user.id);
        resposta.then( (grupo) => {
          let selfUser = this.get('store').peekRecord('user', user.get('id'));
          grupo.data.type = 'grupo';
          let localGrupo = store.push(grupo);
          localGrupo.get('users').pushObject(selfUser);
          this.get('users').removeObject(user);
        }).catch(()=>{
          location.reload();
        });
    }
  }
});
