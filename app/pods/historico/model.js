import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo  } from 'ember-data/relationships';

export default Model.extend({
  tipo: attr('string'),
  titulo: attr('string'),
  descricao: attr('string'),
  ticket: belongsTo('ticket'),
  horario: attr("date"),
  // referencias: [{tipo:"usuario",data:{nome:"pedrinho",id:"789"}},
  // {tipo:"ticket",data:{id:"78"}}]
});
