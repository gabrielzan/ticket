import DS from 'ember-data';
import { belongsTo,hasMany } from 'ember-data/relationships';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';


export default DS.Model.extend({
  dataInclusao: attr('date'),
  cliente: belongsTo('user'),
  produto: belongsTo('produto'),
  status: attr('string'),
  prioridade: attr('string'),
  atribuido: belongsTo('user'),
  grupo: belongsTo('grupo'),
  assunto: attr('string'),
  descricao: attr('string'),
  anexos: attr('string'),
  comentarios: attr('string'),
  historico: hasMany("historico"),
  prioridadeNum: computed('prioridade',function(){
    let retorno = 0;
    switch(this.get("prioridade")){
      case 'Alta':
      retorno = 2 ;
      break;
      case 'Media':
      retorno = 1 ;
      break;
      case 'Baixa':
      retorno = 0 ;
      break;
    }
    return retorno;
  }),
});
