import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany  } from 'ember-data/relationships';

export default Model.extend({
  users: hasMany('user',{inverse: 'grupos'}),
  nome: attr('string'),
  imagem: attr('string'),
});
