import ApplicationAdapter from './application';
import config from '../config/environment';

const { APP:{urlDeRequisicao}} = config;
export default ApplicationAdapter.extend({
  me(){
    let Return = this.ajax('/me', 'GET', {}).then( (user) => {
      return user;
    });
    return Return;
  },
  removeDoGrupo(idgrupo,iduser){
    // var Url = urlDeRequisicao;
    let Return = this.ajax(/*Url*/ "/" + 'intgrup', 'DELETE', {data:{ grupo:{idgrupo},user:{ iduser}}}).then( (resposta) => {
      return resposta;
    })
    return Return;
  },
  adicionaNoGrupo(idgrupo,iduser){
    // var Url = urlDeRequisicao;
    let Return = this.ajax(/*Url*/ "/" + 'intgrup', 'POST', {data:{ grupo:{idgrupo},user:{ iduser}}}).then( (resposta) => {
      return resposta;
    })
    return Return;
  }
});
