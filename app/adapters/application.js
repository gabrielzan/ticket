import DS from 'ember-data';
import TokenAuthorizerMixin from 'ember-simple-auth-token/mixins/token-authorizer';

const { JSONAPIAdapter } = DS;

export default JSONAPIAdapter.extend(TokenAuthorizerMixin, {
});
