import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login',{path:"/"});
  this.route('logged', function() {
    this.route('grupos');
    this.route('newticket');
    this.route('tickets',{path: "/"});
    this.route('ticket', {path: "ticket/:id"});
    this.route('clientes');
    this.route('grupo',{path: "grupo/:id"});
  });
  this.route('clientes');
});

export default Router;
