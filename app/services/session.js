import Session from 'ember-simple-auth/services/session';
import { observer } from '@ember/object';
import { inject } from '@ember/service';
import { task } from 'ember-concurrency';

export default Session.extend({
    store: inject(),
    user: null,
    loadUser: task(function * () {
      // verificar se ja esta logado e se o user n esta preenchido
      let isAuthenticated = this.get('isAuthenticated');
      let user = this.get('user');
      if(isAuthenticated && !user){
        // buscar o user
        let store = this.get('store');
        let userAdapter = store.adapterFor('user');
        let response = yield userAdapter.me();
        //salva-lo na variave user
        let user = response.data.attributes;
        user.id = response.data.id;
        this.set('user',user);
      }
    }).enqueue(),
    atualizaruser(){
      this.set('user',null);
      this.get('loadUser').perform();
    },
    saveUserSession: observer("isAuthenticated",function(){
      if (this.get('session.isAuthenticated')){
        let loadUser = this.get('loadUser');
        loadUser.perform();
      }
    })
});
