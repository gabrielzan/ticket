import Store from 'ember-data/store';
import { get } from '@ember/object';

export default Store.extend({

  didSaveRecord(internalModel) {
    const record = internalModel.getRecord();
    const isDeleted = get(record, 'isDeleted');
    if (!isDeleted) {
      record.eachRelationship((name, { key, kind }) => {
        const relationData = get(record, key);
        if (kind === 'belongsTo') {
          if (!get(relationData, 'id') && relationData.content) {
            relationData.content.destroyRecord();
          }
        } else {
          let i = 0;
          while (i < get(relationData, 'length')) {
            const relationItem = relationData.objectAt(i);
            if (!get(relationItem, 'id')) {
              relationItem.destroyRecord();
            } else {
              i++;
            }
          }
        }
      });
    }

    return this._super(...arguments);
  }

});
